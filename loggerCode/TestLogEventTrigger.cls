@isTest
public with sharing class TestLogEventTrigger {
	@isTest
	static void testCreateLogEvent() {
		Test.startTest();

		List<LogEvent__e> logEventList = new List<LogEvent__e>();

		for (Integer i = 0; i < 100; i++) {
			LogEvent__e logEvent = new LogEvent__e(
				Level__c = 'INFO',
				Message__c = 'msg' + i,
				User__c = 'UserInfo.getUserId()'
			);
			logEventList.add(logEvent);
		}

		List<Database.SaveResult> sr = Eventbus.publish(logEventList);
		Test.getEventBus().deliver();
		System.assertEquals(true, sr.get(0).isSuccess());

		Test.stopTest();
	}
}