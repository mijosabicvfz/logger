@isTest
public with sharing class TestLoggerService {
	@isTest
	static void testLogError() {
		Test.startTest();
		LoggerService.log(LoggingLevel.ERROR, 'Message');
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(1, logs.size(), 'One log should be inserted');
		System.assertEquals('ERROR', logs[0].Level__c, 'Log level should be error');
	}

	@isTest
	static void testDontLogFinest() {
		Test.startTest();
		LoggerService.log(LoggingLevel.FINEST, 'Message');
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(0, logs.size(), 'No logs should be inserted');
	}

	@isTest
	static void testLogException() {
		Test.startTest();
		try {
			Integer error = 1 / 0;
		} catch (Exception e) {
			LoggerService.log(e);
		}
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(1, logs.size(), 'One log should be inserted');
		System.assertEquals('ERROR', logs[0].Level__c, 'Log level should be error');
	}

	static List<Log__c> getLogs() {
		return [SELECT Id, Level__c FROM Log__c];
	}
}